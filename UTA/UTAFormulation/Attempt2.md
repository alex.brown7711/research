---
title: Attempt 2 Description
header-includes:
    - \usepackage[a4paper, margin=0.5in]{geometry}
    - \usepackage{listings}
    - \lstset{breaklines=true}
    - \lstset{language=[Motorola68k]Assembler}
    - \lstset{basicstyle=\small\ttfamily}
    - \lstset{extendedchars=true}
    - \lstset{tabsize=2}
    - \lstset{columns=fixed}
    - \lstset{showstringspaces=false}
    - \lstset{frame=trbl}
    - \lstset{frameround=tttt}
    - \lstset{framesep=4pt}
    - \lstset{numbers=left}
    - \lstset{numberstyle=\tiny\ttfamily}
    - \lstset{postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\color{red}\hook right arrow\space}}}
---

# Grouped Chargers

1. Big M time constraint: 
   
	The initial charge time of the bus next in line ($j$) for the $k^{th}$ visit has to be after the initial charge time of the bus in front ($i$) for the $k^{th}$ visit plus the time it takes to charge for bus $i$. If it is, then $\sigma_{ij}^q = 1$ nullifying the effect of the time horizon variable $T$ (i.e. activating the constraint). If the times overlap ($i$ is not fully to the left of $j$ in the time space diagram) then $\sigma_{ij}^q = 0$ effectively deactivating the constraint. This is solved over bus visit $k$ and over all charging groups $q$.
	
2. Big M space constraint: 
   
	The initial placement of the bus ($j$) has to be below the initial position of the bus in above ($i$) plus the length/width of the vehicle ($i$). If it is, then $\delta_{ij}^q = 1$ nullifying the effect of the charger group length variable $S$ (i.e. activating the constraint). If the vehicles overlap ($i$ is not fully to below $j$ in the time space diagram) then $\delta_{ij}^q = 0$ effectively deactivating the constraint. This is solved over bus visit $k$ and over all charging groups $q$.
	
3. We must guarantee that the bus $i$ is either fully above, below, to the left, or to the right of bus $j$, we sum over the groups to ensure this constraint holds for only one charger.
4. Bus $i$ visit $k$, can either be to the left or right of bus $j$ visit $k$,  but not both on charger group $q$, we sum over the groups to ensure this constraint holds for only one charger.
5. Bus $i$ visit $k$, can either be to above or below bus $j$, visit $k$, but not both on charger group $q$, we sum over the groups to ensure this constraint holds for only one charger.
6. The charging time $p$ plus the starting charge time $u$ must be equal to the time the bus is removed from the charger $c$.
7. Time time bus $i$ is taken off the charger $c$ must be less than or equal to to the time the bus must depart from the station $g$
8. The time of arrival for bus $i$ must be less than or equal to the starting charge time, and the starting charge time must be early enough such that we don't go over the time horizon $T$.
9. The initial charge ($\eta$) of bus $i$, visit $k$ plus the charge time $p$ times the rate of charge per unit time $r$ must be less than or equal to 1.
10. The initial charge ($\eta$) of bus $i$, visit $k$ plus the charge time $p$ times the rate of charge per unit time $r$ minus the amount of time the bus is on route times the rate of discharge per unit time must be greater than 0 (could change this to make a lower bound on allowable charge)
11. The charge at the last step must be greater than or equal to 90%
12. The initial charge for the next ($i+1$) step needs to be the sum of the initial charge from the previous step $i$ plus the amount of charge it gained from the charger minus the amount of discharge from going on route.
13. Bus $i$ is only allowed to be assigned to one charger group per visit

# Unique Chargers
1. Big M time constraint: 
   
	The initial charge time of the bus next in line ($j$) for the $k^{th}$ visit has to be after the initial charge time of the bus in front ($i$) for the $k^{th}$ visit plus the time it takes to charge for bus $i$. If it is, then $\sigma_{ij} = 1$ nullifying the effect of the time horizon variable $T$ (i.e. activating the constraint). If the times overlap ($i$ is not fully to the left of $j$ in the time space diagram) then $\sigma_{ij} = 0$ effectively deactivating the constraint. This is solved over bus visit $k$ and over all chargers $q$.
	
2. Big M space constraint: 
   
	The initial placement of the bus ($j$) has to be below the initial position of the bus in above ($i$) plus the length/width of the vehicle ($i$). If it is, then $\delta_{ij} = 1$ nullifying the effect of the charger group length variable $S = 1$ (i.e. activating the constraint). If the vehicles overlap ($i$ is not fully to below $j$ in the time space diagram) then $\delta_{ij} = 0$ effectively deactivating the constraint. This is solved over bus visit $k$ and over all chargers $q$.

3. We must guarantee that the bus $i$ is either fully above, below, to the left, or to the right of bus $j$, we sum over the groups to ensure this constraint holds for only one charger.
4. Bus $i$ visit $k$, can either be to the left or right of bus $j$ visit $k$, we sum over the groups to ensure this constraint holds for only one charger.
5. Bus $i$ visit $k$, can either be to above or below bus $j$, visit $k$, we sum over the groups to ensure this constraint holds for only one charger.
6. The charging time $p$ plus the starting charge time $u$ must be equal to the time the bus is removed from the charger $c$.
7. Time time bus $i$ is taken off the charger $c$ must be less than or equal to to the time the bus must depart from the station $g$
8. The time of arrival for bus $i$ must be less than or equal to the starting charge time, and the starting charge time must be early enough such that we don't go over the time horizon $T$.
9. The initial charge ($\eta$) of bus $i$, visit $k$ plus the charge time $p$ times the rate of charge per unit time $r$ must be less than or equal to 1.
10. The initial charge ($\eta$) of bus $i$, visit $k$ plus the charge time $p$ times the rate of charge per unit time $r$ minus the amount of time the bus is on route times the rate of discharge per unit time must be greater than 0 (could change this to make a lower bound on allowable charge)
11. The charge at the last step must be greater than or equal to 90%
12. The initial charge for the next ($i+1$) step needs to be the sum of the initial charge from the previous step $i$ plus the amount of charge it gained from the charger minus the amount of discharge from going on route.
13. Bus $i$ is only allowed to be assigned to one charger group per visit
