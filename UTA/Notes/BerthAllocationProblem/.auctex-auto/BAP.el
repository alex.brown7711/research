(TeX-add-style-hook
 "BAP"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "11pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("ulem" "normalem")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art11"
    "inputenc"
    "fontenc"
    "graphicx"
    "grffile"
    "longtable"
    "wrapfig"
    "rotating"
    "ulem"
    "amsmath"
    "textcomp"
    "amssymb"
    "capt-of"
    "hyperref")
   (LaTeX-add-labels
    "sec:org87e64ff"
    "sec:orge517172"
    "sec:org002ecf7"
    "sec:org0f69482"
    "sec:orgc505d53"
    "sec:org91b45f9"
    "sec:org730d115"
    "sec:org116a5ed"
    "sec:orgb596e80"
    "sec:orgdf34972"
    "sec:org611e46b"
    "sec:orgdc9d711"
    "sec:org936677d"
    "sec:orgfb50086"
    "sec:orgcc3b0d3"
    "sec:orgd2f1edb"
    "sec:org3154a33"
    "sec:org92bddd8"
    "sec:org349ce33"
    "sec:org1d5108d"
    "sec:org30a7ed7"
    "sec:org625c984"
    "sec:org86e37ed"
    "sec:org41f5f0f"
    "sec:org76114c1"
    "sec:org0cbdaee"
    "sec:orgd5fae3d"))
 :latex)

