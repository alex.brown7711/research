---
title: Conference Paper Notes
header-includes:
    - \usepackage[a4paper, margin=0.5in]{geometry}
    - \renewcommand{\familydefault}{\sfdefault}
    - \usepackage{listings}
    - \lstset{breaklines=true}
    - \lstset{language=[Motorola68k]Assembler}
    - \lstset{basicstyle=\small\ttfamily}
    - \lstset{extendedchars=true}
    - \lstset{tabsize=2}
    - \lstset{columns=fixed}
    - \lstset{showstringspaces=false}
    - \lstset{frame=trbl}
    - \lstset{frameround=tttt}
    - \lstset{framesep=4pt}
    - \lstset{numbers=left}
    - \lstset{numberstyle=\tiny\ttfamily}
    - \lstset{postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\color{red}\hookrightarrow\space}}}
---

# Structure
* Title
* Abstract
* Keywords
* Introduction
* Background or Literature Review
* Methodology
* Results
* Discussions
* Findings
* Conclusion
* References

# Outline
## Title

## Abstract

## Keywords

## Introduction
* Description of UTA Problem
* Description of Mixed Integer/Linear Programming?
* Related problems 
  	* Increased use of electric vehicles
	* Incorporating electric vehicles into public transport
* Contribution of this paper provides 
  	* Optimal scheduling/packing of vehicles
	* Multiple charger type
	* Multiple vehicle type
	* Vehicle battery dynamics
	* Vehicles that perform routes rather than N new vehicles

## Background/Literature Review
* Brief discussion about network flow as alternative method?
* Extension of PAP
* Fundamental math behind it?
	* Rectangle packing algorithm
* Different methods of BAP
	* Continuous/Discrete
	* Different objective function (minimize wait time, minimize service time 
* Papers to Reference
	* Optimized Scheduling for Solving Position Allocation Problem in Eclectic Vehicle Charging Stations
	* The continuous Berth Allocation Problem in a container terminal with multiple quays

## Methodology
* Using BAP for modeling charging buses (BAP to PAP)
* Example of modeling/results
* Multiple "berths" to mimic different chargers (discretize chargers or group of chargers)
* Battery dynamics
* Discrete or continuous time?
	* Pros/Cons?

## Results
* TBD

## Discussions
* TBD

## Findings
* TBD

## Conclusion

## References
	* Models for the discrete berth allocation problem: A computational comparison
	* Optimized Scheduling for Solving Position Allocation Problem in Eclectic Vehicle Charging Stations
	* The Continuous Berth Allocation Problem in a container terminal with multiple quays
	* Berth Allocation Planning Optimization in Container Terminals
