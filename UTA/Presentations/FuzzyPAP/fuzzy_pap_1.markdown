---
# Front Matter
lang: en-US
title: "The Position Allocation Problem and Fuzzy Linear Programming"
author: "Alexander Brown"
date: \today
 
# Formatting
fontsize: 9pt
linestretch: 1.5
aspectratio: 169 
institude: Utah State University
theme: Rochester
colortheme: beetle
---

# Berth Allocation Problem (BAP)

::: {.columns}
::: {.column}

The Berth Allocation Problem (BAP) is a problem within operations research regarding the allocation of berth space for vessels in container terminals. Vessels arrive over time and the terminal operator needs to assign them to berths to be served (loading and unloading containers) as quickly as possible. Some of the issues addressed in literature are:

- Minimization of vessel total service time
- Minimization of early and delayed departures
- Optimization of vessel arrival times
- Optimization of emissions and fuel consumption

:::

::: {.column}
![](./img/BerthTimeSpace.png)
:::
:::

# BAP to PAP

::: {.columns}
::: {.column}
Because the two processes are similar (but not identical) some effort needs to be made into redefining terms. Another issue to overcome a serial to parallel conversion in the time results (or vectorize the matrix of results).
:::

::: {.column}
![](./img/BAPvsPAP.png)
:::
:::

# Variables
- $S$: the length of the berth
- $T$: the length of the horizon
- $N$: the total number of incoming vehicles
- $p_i$: the charging time for vehicle $i; \forall 1 \leq i \leq N$
- $s_i$: the size of vehicle $i; \forall 1 \leq i \leq N$
- $a_i$: the arrival time for vehicle $i; \forall 1 \leq i \leq N$
- $w_i$: the weight assigned for vehicle $i; \forall 1 \leq i \leq N$

# Decision Variables

- $u_i$: The starting charge time of vehicle $i; \forall 1 \leq i \leq N$
- $v_i$: The starting berth position occupied by vehicle $i; \forall 1 \leq i \leq N$
- $c_i$: The departure time of vehicle $i; \forall 1 \leq i \leq N$

$$
\begin{array}{c}
\sigma_{ij}:\; \begin{cases}
  1 & \textrm{ if vehicle i is full on the left of} \\
    & \textrm{vehicle j in the }                    \\
    & \textrm{Time - space diagram}                 \\
  0 & \textrm{otherwise}                            \\
  \end{cases}                                       \\
\delta_{ij}:\; \begin{cases}
1 & \textrm{ if vehicle i is full below}            \\
  & \textrm{vehicle j in the }                      \\
  & \textrm{Time - space diagram}                   \\
0 & \textrm{otherwise}                              \\
\end{cases}                                         \\
\end{array}
$$

# Formulation

The cost function is of the form

$$
min \sum_{i=1}^N w_i(c_i - a_i)
$$

Subject to the constraints

$$
\begin{array}{cc}
u_j - u_i - p_i - (\sigma_{ij} - 1) T \geq 0                 & \\
v_j - v_i - s_i - (\delta_{ij} - 1) S \geq 0                 & \\
\sigma_{ij} + \sigma_{ji} + \delta_{ij} + \delta_{ji} \geq 1 & \\
\sigma_{ij} + \sigma_{ji} \leq 1                             & \\
\delta_{ij} + \delta_{ji} \leq 1                             & \\
p_i + u_i = c_i                                              & \\
a_i \leq u_i \leq (T-p_i)                                    & \\
\sigma_{ij} \in \{0,1\},\; \delta_{ij} \in \{0,1\} \\
\end{array}
$$

# Big-M Constraints
Big-M constraints are typically used with binary values in conjunction with a "very large" value (usually given the variable $M$). The binary value is used to determine the relevancy of the constraint. For example, if we have the constraint $x - y \leq Mz$ if $z = 0$ then the constraint can be thought as "active" because $x$ must equal $y$. However, if $z = 1$ then the constraint can be thought as "inactive" because $M$ is sufficiently large such that $x - y$ can never be less than $M$.

# Constraint Definition
 - $u_j - u_i - p_i - (\sigma_{ij} - 1) T \geq 0$
   - Starting charge time of bus $j$ must be less than or equal to the starting charge time of $i$ plus the time it takes to charge. Worry about this constraint only if $i$ is to the left of $j$.
 - $v_j - v_i - s_i - (\delta_{ij} - 1) S \geq 0$
   - Starting position of bus $j$ must be less than or equal to the starting position of $i$ plus the time it takes to charge. Worry about this constraint only if $i$ is below $j$.
 - $\sigma_{ij} + \sigma_{ji} + \delta_{ij} + \delta_{ji} \geq 1$
   - Ensure that $i$ or $j$ is either completely to the left of one another of below.
 - $\sigma_{ij} + \sigma_{ji} \leq 1$
   - Only $i$ or $j$ can be to the left of the other. Used to avoid allocating multiple spots for the vehicle.

# Constraint Definition
 - $\delta_{ij} + \delta_{ji} \leq 1$
   - Only $i$ or $j$ can be to below of the other. Used to avoid allocating multiple spots for the vehicle.
 - $p_i + u_i = c_i$
   - Charge time + Time of arrival = departure time
 - $a_i \leq u_i \leq (T-p_i)$
   - Time of arrival must be less than or equal to charge start time which must be less than or equal to time horizon minus the charging time
   - If $p_i = T$ then $u_i = 0$
 - $\sigma_{ij} \in \{0,1\},\; \delta_{ij} \in \{0,1\}$
   - Valid values of $\sigma$ and $\delta$

# Example

![](./img/PAPExample.png)

# Fuzzy Linear Programming
## Definitions
## Concepts
## Properties
## Fuzzy Sets
## Triangular Fuzzy Numbers
## Fuzzy Arithmetic
## Comparison of Fuzzy Numbers

# Definitions
- *Fuzzy Subset*: Let $E$ be a set that is finite or infinite. Let $A$ be a set contained in $E$. The order pair $(x, \mu_{A}(x))$ defines the fuzzy subset $A$ of $E$, where $x$ is an element in $E$ and $\mu_{A}(x)$ is the degree of membership of $x$ in $E$.
- *Fuzzy Number*: A fuzzy number is a generalization of a regular, real number. It refers to a connected set of possible values, where each possible value has its own weight between 0 and 1. A fuzzy number is thus a special case of a convex, normalized fuzzy set of the real line.
- *Triangular Fuzzy Numbers*: A fuzzy number $\tilde{A} = (a,b,c)$ is called a triangular fuzzy number if its membership function is given by

$$
\mu_{\tilde{A}(x)} =
\begin{cases}
0               & x < a           \\
\frac{x-a}{b-a} & a \leq x \leq b \\
\frac{c-x}{c-b} & b \leq x \leq c \\
0               & x > c           \\
\end{cases}
$$

# Concepts
$c$, $j$, and $a$ are natural numbers and satisfy Pythagorean's identity $c^2 + j^2 = a^2$. Such numbers are called a Pythagorean triple (or triangle). Without loss of generality we can also state that we can always find two natural numbers $j_1$ and $j_2$ such that

1. $g.c.d\; (j_1, j_2) = 1$
2. $c = j_{2}^2 - j_{1}^2$, $2j_{1}j_{2}$ and $a = j_{2}^2 + j_{1}^2$
3. $j_2 > j_1 > 0 \#$

Let $A = (a,b,c,d)$ be a trapezoidal fuzzy number. Then, its $\lambda$ -cut. We then get that $A^{\lambda} = [a + (b-a)\lambda, d-(d-c)]$ where $0 \leq \lambda \leq 1$. If we then allow $b = c$ we regain the triangular fuzzy number.

# Properties
1. If $[c,j,a]$ is a Pythagorean Triangular Fuzzy Number, then $[mc,mj,ma]$ is also a Pythagorean Triangular Fuzzy Number
2. If $d$ is a common divisor of $c$ and $j$, then $d$ divides $a$
3. Let $e$ denote a common divisor of $c$, $j$, $a$. Then $[c_1,j_1,a_1]$ such that $c_1 = c/e$, $j_1 = j/e$, $a_1 = a/e$, is also a Pythagorean Triangular Fuzzy Number.
4. All Pythagorean Triangular Fuzzy Numbers like $[c,j,a]$ are multiples of $[c_1,j_1,a_1]$.

# Fuzzy Sets
$$
    \tilde{A} = \{ (x, \mu_{\tilde{A}}(x), x \in X) \}
$$

Where $\mu_{\tilde{A}} : X \rightarrow [0,1]$ is called the membership function.

# Triangular Fuzzy Numbers
A triangular fuzzy number is represented as $\tilde{a} = (a_1, a_2, a_3)$

![](./img/TriangularFuzzyNumber.png)

# Fuzzy Arithmetic
If we have two fuzzy numbers $\tilde{a}$ and $\tilde{b}$ then the sum is taken as

$$
    \tilde{a} + \tilde{b} = (a_1 + b_1, a_2 + b_2, a_3 + b_3)
$$

The difference is found as followed:
$$
    \tilde{a} - \tilde{b} = (a_1 - b_3, a_2 - b_2, a_3 - b_1)
$$


The product of two fuzzy values is defined as follows:

$$
    \tilde{a} * \tilde{b} = (a_1 * b_1, a_2 * b_2, a_3 * b_3)
$$

# Comparison of Fuzzy Numbers
There are multiple ways of comparing fuzzy numbers, the way done in this particular text is the First Index of Tagger

$$
\mathbb{R} = \frac{a_1 + a_2 + a_3}{3}
$$

Which results in $\tilde{a} \leq \tilde{b}$ when $\mathbb{R}(\tilde{a}) \leq \mathbb{R}(\tilde{b})$

# Formulation of Fully Fuzzy Linear Programs
The Fuzzy Linear Program can be written as:

$$
\begin{array}{cc}
max & \sum_{j=1}^n \tilde{c}_j \tilde{x}_j                     \\
s.t. & \sum_{j=1}^n \tilde{a}_{ij} \tilde{x}_j \leq \tilde{b}_j \\
\end{array}
$$

where $\tilde{c}$, $\tilde{a_{ij}}$, and $b_j$ are parameters and $x_j$ is the non-negative decision variable.

# Formulation of Fully Fuzzy Linear Programs
::: {.columns}
:::{.column}
As a consequence of the mathematics applied to fuzzy numbers, we can rewrite this problem as a standard linear problem by applying the addition/multiplication rules.

The results shown on the right can be interpreted as follows: 

\

> Maximize the average result which in turn is maximizing the lower, average, and upper bound of the fuzzy number.
:::

:::{.column}
$\mathbb{R}$ is an ordering function defined as

$$
\mathbb{R}(\tilde{a}) = \frac{a1 + a2 + a3}{3}
$$

![](./img/FuzzyToStandardBAP.png)
:::
:::

# FFLP Model for the Berth Allocation Problem
::: {.columns}
:::{.column}
Using the following definitions:

- $z_{ij}^x$ indicates if vessel $i$ is to the left of vessel $j$ at quay $x$.
- $z_{ij}^y$ indicates if vessel $i$ berths before vessel $j$ at quay $y$.
- $M$ is a very large number
:::

:::{.column}
![](./img/FFLPDef.png)
:::
:::

# FFLP Model for the Berth Allocation Problem
::: {.columns}
:::{.column}
The Berth Allocation Problem can be formulated as shown to the right where the $z$ variable is similar to the $\sigma$ variable in the PAP problem. It indicates if the vessel $i$ is located to the left of vessel $j$ at the berth.
:::
:::{.column}
![](./img/FFLPFormulation.png)
:::
:::

# FFLP Model for the Berth Allocation Problem
::: {.columns}
:::{.column}
Applying the methodology from the previous section, the BAP problem can be fully modeled assigned
:::

:::{.column}
![](./img/FFLPMILP.png)
:::
:::

# Sources
## BAP
* [The dynamic berth allocation problem for a container port](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.471.9196&rep=rep1&type=pdf)
* [Berth allocation problem](https://en.wikipedia.org/wiki/Berth_allocation_problem)
* [Berth Allocation Planning Optimization in Container Terminals](https://people.orie.cornell.edu/jdai/publications/daiLinMoorthyTeo08.pdf)
* [Berth Allocation Problem – A Case Study in Algorithmic Approaches](https://www.comp.nus.edu.sg/~cs5234/BAP/lecture/BAP-Lecture-CS5234.pdf)
* [A Fully Fuzzy Linear Programming Model to the Berth Allocation](https://www.researchgate.net/publication/320013147_A_Fully_Fuzzy_Linear_Programming_Model_to_the_Berth_Allocation_Problem)
* [The Continuous Berth Allocation Problem In A Container Terminal With Multiple Quays](https://www.sciencedirect.com/science/article/pii/S0957417415003462)

# Sources
## PAP
* [Optimized Scheduling for Solving Position Allocation Problem in Electric Vehicle Charging Stations](https://ieeexplore.ieee.org/document/8786524)

# Sources
## Fuzzy Linear Programming
* [A Fully Fuzzy Linear Programming Model to the Berth Allocation Problem](https://git.brownhaus.xyz/School/Spring2021/src/branch/master/FundamentalsOfResourceAllocation/Literature/FuzzyBAP.pdf)

# Sources
## Fuzzy Theory
* [An Introduction to Fuzzy Linear Programming Problems](https://www.sciencedirect.com/science/article/pii/0022247X83902536)
* [Fuzzy Numbers](https://www.sciencedirect.com/science/article/pii/0022247X83902536)
* [Theory of Triangular Fuzzy Number](https://www.researchgate.net/publication/318946539_Theory_of_Triangular_Fuzzy_Number)

# Sources
## Ranking Functions
* [On Ranking Functions of Single-Path Linear-Constraint Loops](https://link-springer-com.dist.lib.usu.edu/content/pdf/10.1007/s10009-019-00549-9.pdf)
* [Eventual Linear Ranking Functions](https://www.bugseng.com/sites/default/files/BagnaraM13PPDP.pdf)
* [Ranking Fuzzy Subsets Over The Unit Interval](https://ieeexplore-ieee-org.dist.lib.usu.edu/document/4046341)
* [A Procedure For Ordering Fuzzy Subsets Of The Unit Interval](https://www-scopus-com.dist.lib.usu.edu/record/display.uri?eid=2-s2.0-0019585325&origin=resultslist&sort=plf-f&src=s&sid=e04fd19ef279878a8a73c1579d0e5e52&sot=b&sdt=b&sl=74&s=TITLE-ABS-KEY%28A+procedure+for+ordering+fuzzy+subsets+of+the+unit+interval%29&relpos=0&citeCnt=963&searchTerm=)
* [Learning Linear Ranking Functions for Beam Search with Applications to Planning](https://jmlr.csail.mit.edu/papers/volume10/xu09c/xu09c.pdf)
