% Created 2021-06-21 Mon 19:56
% Intended LaTeX compiler: pdflatex
\documentclass[aspectratio=169]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\AtBeginSection[]{\begin{frame}<beamer>\frametitle{Topic}\tableofcontents[currentsection]\end{frame}}
\usetheme[height=20pt]{Rochester}
\usecolortheme{beetle}
\author{Alexander Brown}
\date{\today}
\title{The Position Allocation Problem and Fuzzy Linear Programming}
\hypersetup{
 pdfauthor={Alexander Brown},
 pdftitle={The Position Allocation Problem and Fuzzy Linear Programming},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.2 (Org mode 9.5)}, 
 pdflang={English}}
\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}


\section{Position Allocation Problem}
\label{sec:org1150be2}
\begin{frame}[label={sec:orgfd29a66}]{Berth Allocation Problem (BAP)}
\begin{columns}
\begin{column}{0.60\columnwidth}
\begin{block}{\(\;\)}
The Berth Allocation Problem  (BAP) is a problem within operations research regarding the allocation of berth space for vessels in container terminals. Vessels arrive over time and the terminal operator needs to assign them to berths to be served (loading and unloading containers) as quickly as possible. Some of the issues addressed in literature are:

\begin{itemize}
\item Minimization of vessel total service time
\item Minimization of early and delayed departures
\item Optimization of vessel arrival times
\item Optimization of emissions and fuel consumption
\end{itemize}
\end{block}
\end{column}

\begin{column}{0.50\columnwidth}
\begin{block}{\(\;\)}
\begin{center}
\includegraphics[width=.9\linewidth]{./img/BerthTimeSpace.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org2fa30e9}]{BAP to PAP}
\begin{columns}
\begin{column}{0.68\columnwidth}
\begin{block}{\(\;\)}
Because the two processes are similar (but not identical) some effort needs to be made into redefining terms. Another issue to overcome a serial to parallel conversion in the time results (or vectorize the matrix of results).
\end{block}
\end{column}

\begin{column}{0.45\columnwidth}
\begin{block}{\(\;\)}
\begin{center}
\includegraphics[width=.9\linewidth]{./img/BAPvsPAP.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:orgbd19bed}]{Variables}
\begin{itemize}
\item \(S\): the length of the berth
\item \(T\): the length of the horizon
\item \(N\): the total number of incoming vehicles
\item \(p_i\): the charging time for vehicle \(i; \forall 1 \leq i \leq N\)
\item \(s_i\): the size of vehicle \(i; \forall 1 \leq i \leq N\)
\item \(a_i\): the arrival time for vehicle \(i; \forall 1 \leq i \leq N\)
\item \(w_i\): the weight assigned for vehicle \(i; \forall 1 \leq i \leq N\)
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orge1904cf}]{Decision Variables}
\begin{itemize}
\item \(u_i\): The starting charge time of vehicle \(i; \forall 1 \leq i \leq N\)
\item \(v_i\): The starting berth position occupied by vehicle \(i; \forall 1 \leq i \leq N\)
\item \(c_i\): The departure time of vehicle \(i; \forall 1 \leq i \leq N\)
\end{itemize}

\begin{equation*}
\begin{array}{c}
\sigma_{ij}:\; \begin{cases}
  1 & \textrm{ if vehicle i is full on the left of} \\
     & \textrm{vehicle j in the } \\
      & \textrm{Time - space diagram} \\
  0 & \textrm{otherwise} \\
  \end{cases} \\
\delta_{ij}:\; \begin{cases}
1 & \textrm{ if vehicle i is full below} \\
   & \textrm{vehicle j in the } \\
   & \textrm{Time - space diagram} \\
0 & \textrm{otherwise} \\
\end{cases}\\
\end{array}
\end{equation*}
\end{frame}

\begin{frame}[label={sec:org0b92ba6}]{Formulation}
The cost function is of the form
\begin{equation}
min \sum_{i=1}^N w_i(c_i - a_i)
\end{equation}

Subject to the constraints

\begin{equation}
\begin{array}{cc}
u_j - u_i - p_i - (\sigma_{ij} - 1) T \geq 0                 & \\
v_j - v_i - s_i - (\delta_{ij} - 1) S \geq 0                 & \\
\sigma_{ij} + \sigma_{ji} + \delta_{ij} + \delta_{ji} \geq 1 & \\
\sigma_{ij} + \sigma_{ji} \leq 1                             & \\
\delta_{ij} + \delta_{ji} \leq 1                             & \\
p_i + u_i = c_i                                              & \\
a_i \leq u_i \leq (T-p_i)                                    & \\
\sigma_{ij} \in \{0,1\},\; \delta_{ij} \in \{0,1\} \\
\end{array}
\end{equation}
\end{frame}

\begin{frame}[label={sec:orgc14edd5}]{Big-M Constraints}
Big-M constraints are typically used with binary values in conjunction with a ``very large'' value (usually given the variable \(M\)). The binary value is used to determine the relevancy of the constraint. For example, if we have the constraint \(x - y \leq Mz\) if \(z = 0\) then the constraint can be thought as ``active'' because \(x\) must equal \(y\). However, if \(z = 1\) then the constraint can be thought as ``inactive'' because \(M\) is sufficiently large such that \(x - y\) can never be less than \(M\).
\end{frame}

\begin{frame}[label={sec:orge16d349}]{Constraint Definition}
\begin{itemize}
\item \(u_j - u_i - p_i - (\sigma_{ij} - 1) T \geq 0\)
\begin{itemize}
\item Starting charge time of bus \(j\) must be less than or equal to the starting charge time of \(i\) plus the time it takes to charge. Worry about this constraint only if \(i\) is to the left of \(j\).
\end{itemize}
\item \(v_j - v_i - s_i - (\delta_{ij} - 1) S \geq 0\)
\begin{itemize}
\item Starting position of bus \(j\) must be less than or equal to the starting position of \(i\) plus the time it takes to charge. Worry about this constraint only if \(i\) is below \(j\).
\end{itemize}
\item \(\sigma_{ij} + \sigma_{ji} + \delta_{ij} + \delta_{ji} \geq 1\)
\begin{itemize}
\item Ensure that \(i\) or \(j\) is either completely to the left of one another of below.
\end{itemize}
\item \(\sigma_{ij} + \sigma_{ji} \leq 1\)
\begin{itemize}
\item Only \(i\) or \(j\) can be to the left of the other. Used to avoid allocating multiple spots for the vehicle.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org6f06074}]{Constraint Definition}
\begin{itemize}
\item \(\delta_{ij} + \delta_{ji} \leq 1\)
\begin{itemize}
\item Only \(i\) or \(j\) can be to below of the other. Used to avoid allocating multiple spots for the vehicle.
\end{itemize}
\item \(p_i + u_i = c_i\)
\begin{itemize}
\item Charge time + Time of arrival = departure time
\end{itemize}
\item \(a_i \leq u_i \leq (T-p_i)\)
\begin{itemize}
\item Time of arrival must be less than or equal to charge start time which must be less than or equal to time horizon minus the charging time
\item If \(p_i = T\) then \(u_i = 0\)
\end{itemize}
\item \(\sigma_{ij} \in \{0,1\},\; \delta_{ij} \in \{0,1\}\)
\begin{itemize}
\item Valid values of \(\sigma\) and \(\delta\)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org3d3dd8f}]{Example}
\begin{center}
\includegraphics[width=0.45\textwidth]{./img/PAPExample.png}
\end{center}
\end{frame}

\section{Fuzzy Linear Programming}
\label{sec:org651ccdb}
\begin{frame}[label={sec:org059f8be}]{Outline}
\begin{block}{Definitions}
\end{block}
\begin{block}{Concepts}
\end{block}
\begin{block}{Properties}
\end{block}
\begin{block}{Fuzzy Sets}
\end{block}
\begin{block}{Triangular Fuzzy Numbers}
\end{block}
\begin{block}{Fuzzy Arithmetic}
\end{block}
\begin{block}{Comparison of Fuzzy Numbers}
\end{block}
\end{frame}

\begin{frame}[label={sec:orgc36458d}]{Definitions}
\begin{itemize}
\item \alert{Fuzzy Subset}: Let \(E\) be a set that is finite or infinite. Let \(A\) be a set contained in \(E\). The order pair \((x, \mu_{A}(x))\) defines the fuzzy subset \(A\) of \(E\), where \(x\) is an element in \(E\) and \(\mu_{A}(x)\) is the degree of membership of \(x\) in \(E\).
\item \alert{Fuzzy Number}: A fuzzy number is a generalization of a regular, real number. It refers to a connected set of possible values, where each possible value has its own weight between 0 and 1. A fuzzy number is thus a special case of a convex, normalized fuzzy set of the real line.
\item \alert{Triangular Fuzzy Numbers}: A fuzzy number \(\tilde{A} = (a,b,c)\) is called a triangular fuzzy number if its membership function is given by

\begin{equation}
\mu_{\tilde{A}(x)} =
\begin{cases}
    0               & x < a           \\
    \frac{x-a}{b-a} & a \leq x \leq b \\
    \frac{c-x}{c-b} & b \leq x \leq c \\
    0               & x > c           \\
\end{cases}
\end{equation}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org442516a}]{Concepts}
\(c\), \(j\), and \(a\) are natural numbers and satisfy Pythagorean's identity \(c^2 + j^2 = a^2\). Such numbers are called a Pythagorean triple (or triangle). Without loss of generality we can also state that we can always find two natural numbers \(j_1\) and \(j_2\) such that

\begin{enumerate}
\item \(g.c.d\; (j_1, j_2) = 1\)
\item \(c = j_{2}^2 - j_{1}^2\), \(2j_{1}j_{2}\) and \(a = j_{2}^2 + j_{1}^2\)
\item \(j_2 > j_1 > 0*\)
\end{enumerate}

Let \(A = (a,b,c,d)\) be a trapezoidal fuzzy number. Then, its \(\lambda\) -cut. We then get that \(A^{\lambda} = [a + (b-a)\lambda, d-(d-c)]\) where \(0 \leq \lambda \leq 1\). If we then allow \(b = c\) we regain the triangular fuzzy number.
\end{frame}

\begin{frame}[label={sec:org7ee7e15}]{Properties}
\begin{enumerate}
\item If \([c,j,a]\) is a Pythagorean Triangular Fuzzy Number, then \([mc,mj,ma]\) is also a Pythagorean Triangular Fuzzy Number
\item If \(d\) is a common divisor of \(c\) and \(j\), then \(d\) divides \(a\)
\item Let \(e\) denote a common divisor of \(c\), \(j\), \(a\). Then \([c_1,j_1,a_1]\) such that \(c_1 = c/e\), \(j_1 = j/e\), \(a_1 = a/e\), is also a Pythagorean Triangular Fuzzy Number.
\item All Pythagorean Triangular Fuzzy Numbers like \([c,j,a]\) are multiples of \([c_1,j_1,a_1]\).
\end{enumerate}
\end{frame}

\begin{frame}[label={sec:org7311f21}]{Fuzzy Sets}
\begin{equation}
    \tilde{A} = \{ (x, \mu_{\tilde{A}}(x), x \in X) \}
\end{equation}

Where \(\mu_{\tilde{A}} : X \rightarrow [0,1]\) is called the membership function.
\end{frame}

\begin{frame}[label={sec:orgc9556e0}]{Triangular Fuzzy Numbers}
A triangular fuzzy number is represented as \(\tilde{a} = (a_1, a_2, a_3)\)

\begin{center}
\includegraphics[width=0.45\textwidth]{./img/TriangularFuzzyNumber.png}
\end{center}
\end{frame}

\begin{frame}[label={sec:org97063e8}]{Fuzzy Arithmetic}
If we have two fuzzy numbers \(\tilde{a}\) and \(\tilde{b}\) then the sum is taken as

\begin{equation}
    \tilde{a} + \tilde{b} = (a_1 + b_1, a_2 + b_2, a_3 + b_3)
\end{equation}

And similarly is done for the difference.

The product of two fuzzy values is defined as follows:

\begin{equation}
    \tilde{a} * \tilde{b} = (a_1 * b_1, a_2 * b_2, a_3 * b_3)
\end{equation}
\end{frame}

\begin{frame}[label={sec:org0490817}]{Comparison of Fuzzy Numbers}
There are multiple ways of comparing fuzzy numbers, the way done in this particular text is the First Index of Tagger

\begin{equation}
\mathbb{R} = \frac{a_1 + a_2 + a_3}{3}
\end{equation}

Which results in \(\tilde{a} \leq \tilde{b}\) when \(\mathbb{R}(\tilde{a}) \leq \mathbb{R}(\tilde{b})\)
\end{frame}

\section{Fuzzy Berth Allocation Problem}
\label{sec:org8c710f0}

\begin{frame}[label={sec:orgcd09e69}]{Formulation of Fully Fuzzy Linear Programs}
The Fuzzy Linear Program can be written as:

\begin{equation}
\begin{array}{cc}
max & \sum_{j=1}^n \tilde{c}_j \tilde{x}_j \\
s.t. & \sum_{j=1}^n \tilde{a}_{ij} \tilde{x}_j \leq \tilde{b}_j \\
\end{array}
\end{equation}
\end{frame}

\begin{frame}[label={sec:orgb347dec}]{Formulation of Fully Fuzzy Linear Programs}
\begin{columns}
\begin{column}{0.4\columnwidth}
\begin{block}{\(\;\)}
As a consequence of the mathematics applied to fuzzy numbers, we can rewrite this problem as a standard linear problem by applying the addition rule:
\end{block}
\end{column}

\begin{column}{0.68\columnwidth}
\begin{block}{\(\;\)}
\begin{center}
\includegraphics[width=.9\linewidth]{./img/FuzzyToStandardBAP.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org6b7fc5b}]{FFLP Model for the Berth Allocation Problem}
\begin{columns}
\begin{column}{0.4\columnwidth}
\begin{block}{\(\;\)}
Using the following definitions:

\begin{itemize}
\item \(z_{ij}^x\) indicates if vessel \(i\) is to the left of vessel \(j\) at quay \(x\).
\item \(z_{ij}^y\) indicates if vessel \(i\) berths before vessel \(j\) at quay \(y\).
\item \(M\) is a very large number
\end{itemize}
\end{block}
\end{column}

\begin{column}{0.68\columnwidth}
\begin{block}{\(\;\)}
\begin{center}
\includegraphics[width=0.75\textwidth]{./img/FFLPDef.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[label={sec:org7643691}]{FFLP Model for the Berth Allocation Problem}
\begin{columns}
\begin{column}{0.4\columnwidth}
\begin{block}{\(\;\)}
The Berth Allocation Problem can be formulated as shown to the right where the \(z\) variable is similar to the \(\sigma\) variable in the PAP problem. It indicates if the vessel \(i\) is located to the left of vessel \(j\) at the berth.
\end{block}
\end{column}

\begin{column}{0.5\columnwidth}
\begin{block}{\(\;\)}
\begin{center}
\includegraphics[width=0.75\textwidth]{./img/FFLPFormulation.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}


\begin{frame}[label={sec:org1c69b00}]{FFLP Model for the Berth Allocation Problem}
\begin{columns}
\begin{column}{0.4\columnwidth}
\begin{block}{\(\;\)}
Applying the methodology from the previous section, the BAP problem can be fully modeled assigned
\end{block}
\end{column}

\begin{column}{0.5\columnwidth}
\begin{block}{\(\;\)}
\begin{center}
\includegraphics[width=0.45\textwidth]{./img/FFLPMILP.png}
\end{center}
\end{block}
\end{column}
\end{columns}
\end{frame}

\section{Sources}
\label{sec:org1c5a45b}
\begin{frame}[label={sec:orgc26f73d}]{BAP}
\begin{itemize}
\item \href{http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.471.9196\&rep=rep1\&type=pdf}{The dynamic berth allocation problem for a container port}
\item \href{https://en.wikipedia.org/wiki/Berth\_allocation\_problem}{Berth allocation problem}
\item \href{https://people.orie.cornell.edu/jdai/publications/daiLinMoorthyTeo08.pdf}{Berth Allocation Planning Optimization in Container Terminals}
\item \href{https://www.comp.nus.edu.sg/\~cs5234/BAP/lecture/BAP-Lecture-CS5234.pdf}{Berth Allocation Problem – A Case Study in Algorithmic Approaches}
\item \href{https://www.researchgate.net/publication/320013147\_A\_Fully\_Fuzzy\_Linear\_Programming\_Model\_to\_the\_Berth\_Allocation\_Problem}{A Fully Fuzzy Linear Programming Model to the Berth Allocation}
\item \href{https://www.sciencedirect.com/science/article/pii/S0957417415003462}{The Continuous Berth Allocation Problem In A Container Terminal With Multiple Quays}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org88d5047}]{PAP}
\begin{itemize}
\item \href{https://ieeexplore.ieee.org/document/8786524}{Optimized Scheduling for Solving Position Allocation Problem in Electric Vehicle Charging Stations}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org87723f5}]{Fuzzy Linear Programming}
\begin{itemize}
\item \href{https://git.brownhaus.xyz/School/Spring2021/src/branch/master/FundamentalsOfResourceAllocation/Literature/FuzzyBAP.pdf}{A Fully Fuzzy Linear Programming Model to the Berth Allocation Problem}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:org65d1c7e}]{Fuzzy Theory}
\begin{itemize}
\item \href{https://www.sciencedirect.com/science/article/pii/0022247X83902536}{An Introduction to Fuzzy Linear Programming Problems}
\item \href{https://www.sciencedirect.com/science/article/pii/0022247X83902536}{Fuzzy Numbers}
\item \href{https://www.researchgate.net/publication/318946539\_Theory\_of\_Triangular\_Fuzzy\_Number}{Theory of Triangular Fuzzy Number}
\end{itemize}
\end{frame}

\begin{frame}[label={sec:orgaf3abbf}]{Ranking Functions}
\begin{itemize}
\item \href{https://link-springer-com.dist.lib.usu.edu/content/pdf/10.1007/s10009-019-00549-9.pdf}{On Ranking Functions of Single-Path Linear-Constraint Loops}
\item \href{https://www.bugseng.com/sites/default/files/BagnaraM13PPDP.pdf}{Eventual Linear Ranking Functions}
\item \href{https://ieeexplore-ieee-org.dist.lib.usu.edu/document/4046341}{Ranking Fuzzy Subsets Over The Unit Interval}
\item \href{https://www-scopus-com.dist.lib.usu.edu/record/display.uri?eid=2-s2.0-0019585325\&origin=resultslist\&sort=plf-f\&src=s\&sid=e04fd19ef279878a8a73c1579d0e5e52\&sot=b\&sdt=b\&sl=74\&s=TITLE-ABS-KEY\%28A+procedure+for+ordering+fuzzy+subsets+of+the+unit+interval\%29\&relpos=0\&citeCnt=963\&searchTerm=}{A Procedure For Ordering Fuzzy Subsets Of The Unit Interval}
\item \href{https://jmlr.csail.mit.edu/papers/volume10/xu09c/xu09c.pdf}{Learning Linear Ranking Functions for Beam Search with Applications to Planning}
\end{itemize}
\end{frame}
\end{document}
