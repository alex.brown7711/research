# Optimal Estimation Project Description

## [Overview](https://usu.instructure.com/courses/675526/files/folder/00_misc?preview=81747027)
### Problem Description
The next decade will see several developments in the establishment of a long-term human presence on  the  moon. The  NASA  Gateway  programs  seeks  to  establish  an  outpost  orbiting  the  moon,  to  enable  repeat  lunar missions and serve as a staging point for deep space exploration.  In the event of an emergency, **a vehicle may need to return rapidly from the lunar surface to the Gateway**.  An important part of a successful return is the determination of the vehicle velocity, after the powered ascent maneuver, to less than 10% of the Delta V required for the transfer maneuver. To support non-line-of-sight missions, this must take place without the need for communication with the Gateway.

Important Points:
* NASA Gateway orbits the moon
* Vehicle on the surface to return to the Gateway
*

### Objective
Develop a terrain-relative navigation system based on the EKF, which processes line-of-sight measurements to known landmarks (e.g. craters) immediately following the powered ascent maneuver. Analyze the convergence of velocity error to within 10% of the Delta V required to perform a Hohman transfer to 3000 km altitude above the lunar surface, corresponding to the perigee of the Gateway.

Important Points:
* Hohmann Transfer (see link in Notes)
	* Delta V: The velocity change to change orbit radius
	* Need analyze the convergence of velocity error within 10% of Delta V required
	* Hohmann transfer to 3000 km corresponding to perigee of Gateway

# [What is the Gateway](https://www.nasa.gov/gateway/overview)
The Gateway will be an outpost orbiting the Moon that provides vital support for a long-term human return to the lunar surface, as well as a staging point for deep space exploration. It is a critical component of NASA’s Artemis program.

The Gateway is a vital part of NASA’s deep space exploration plans, along with the Space Launch System (SLS) rocket, Orion spacecraft, and human landing system that will send astronauts to the Moon. Gaining new experiences on and around the Moon will prepare NASA to send the first humans to Mars in the coming years, and the Gateway will play a vital role in this process. It is a destination for astronaut expeditions and science investigations, as well as a port for deep space transportation such as landers en route to the lunar surface or spacecraft embarking to destinations beyond the Moon.

NASA has focused Gateway development on the initial critical elements required to support the 2024 landing – the Power and Propulsion Element, the Habitation and Logistics Outpost (HALO) and logistics capabilities.

![Image of the Gateway](https://www.nasa.gov/sites/default/files/thumbnails/image/gateway_hero_angles_000.png)

# Hohmann Transfer
In oribinal mechanics, the Hohmann transfer orbi is an elliptical orbit used to transfer between two circular orbits of different radii around a central body in the same plane. The Hohmann transfer often uses the lowest possible amount of porpellant in traveling between these orbits. A Hohmann transfer orbit also determines a fixed time required to travel between the starting and destination points. When transfer is performed between orbits close to celestial bodies with significant gravitation, much less delta-v is usually required, as the Oberth effect may be employed for the burns.

![Example of Hohmann transfer orbit. Orbit 1 (circular) is propelled into eliptical orbit (2) and becomes circular orbit (3)](https://upload.wikimedia.org/wikipedia/commons/d/df/Hohmann_transfer_orbit.svg)

The Hohmann transfer orbit is based on two instantaneous velocity changes. Extra fuel is required to compenstate for the fact that the bursts take time; this is minimized by using high-thrust engines to minimize the duration of the bursts. For transfers in Earth orbit, the two burns are labelled the *perigee burn* and the *apogee burn*. Alternately, the second burn is to curcularize the orbit may be referred to as a *circularization burn*.

An orbit which traverses less than 180° around the primary is called a "Type I" Hohmann transfer, while an orbit which traverses more than 180° is called a "Type II" Hohmann transfer.

## Calculation
For a small body orbiting another large body, the total energy of the smaller body is the sum of its kinetic and potential energy, and this total energy also equals half the potential at the average distance $`a`$ (the semi0major axis).

```math
E = \frac{mv^2}{2} - \frac{GMm}{r} = -\frac{Gmm}{2a}
```

Solving for $`v`$ results in:

```math
v^2 = \mu (2/r - 1/a)
```

Where:
* $`v`$ is the speed of an oribing body
* $`\mu=GM`$ is the standard gravitational parameter of the primary body, assuming $`M+m`$ is not significantly bigger than $`M`$
* $`r`$ is the distance of the orbiting body from the primary focus
* $`a`$ is the semi-major axis of the body's orbit (think elpises)

Therefore, the $`\Delta v`$ required for the Hohmann transfer can be computed as follows under the assumption on instantaneous impulses:

```math
\Delta v_1 = \sqrt{\frac{\mu}{r_1}} \Big( \sqrt{\fra{2r_2}{r_1 + r_2}} - 1 \Big)
```

to enter eliptical orbit at $`r = r_1`$ from the $`r_1`$ circular orbit, and

```math
\Delta v_2 = \sqrt{\frac{\mu}{r_2}} \Big( \sqrt{\fra{2r_2}{r_1 + r_2}} - 1 \Big)
```

to leave the elliptical orbit at $`r = r_2`$ to the $`r_2`$ circular orbit, where $`r_1`$ and $`r_2`$ are respectively the radii of the departure and arrival circular orbits. Refer to the following image for when the $`\Delta v`$ are applied.

![Example of the locations when $`\Delta v`$ are applied](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8c/Hohmann_transfer_orbit2.svg/220px-Hohmann_transfer_orbit2.svg.png)

Where the total $`\Delta v`$ required can be expressed as:

```math
\Delta v_{total} = \Delta v_1 + \Delta v_2
```

Whther moving into a higher or lower orbit, by [Kepler's third lay](https://en.wikipedia.org/wiki/Kepler%27s_laws_of_planetary_motion#Kepler's_understanding_of_the_laws), the time taken to transfer the orbit is represented as

```math
t_H = 1/2 \sqrt{\frac{4 \pi^2 a_{H}^3}{\mu}} = \pi \sqrt{\frac{(r_1 + r_2)^3}{8 \mu}}
```

where $`a_H`$ is the length of the semi-marjor axis. of the Hohmann transfer orbit.

## Notes
* Perigee of Gateway: The point in an orbit around a planet where the orbiting body is closest to the planet.
* [Line of Sight Propogation](https://en.wikipedia.org/wiki/Line-of-sight_propagation)
* Hohmann Transfer](https://en.wikipedia.org/wiki/Hohmann_transfer_orbit)
* [Semi-major axis](https://en.wikipedia.org/wiki/Semi-major_and_semi-minor_axes)
