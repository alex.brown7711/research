---
title: Terrain Relative Navigation Paper Notes
header-includes:
    - \usepackage[a4paper, margin=0.5in]{geometry}
    - \usepackage{listings}
    - \lstset{breaklines=true}
    - \lstset{language=[Motorola68k]Assembler}
    - \lstset{basicstyle=\small\ttfamily}
    - \lstset{extendedchars=true}
    - \lstset{tabsize=2}
    - \lstset{columns=fixed}
    - \lstset{showstringspaces=false}
    - \lstset{frame=trbl}
    - \lstset{frameround=tttt}
    - \lstset{framesep=4pt}
    - \lstset{numbers=left}
    - \lstset{numberstyle=\tiny\ttfamily}
    - \lstset{postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\color{red}\hookrightarrow\space}}}
---

# Hohmann Transfer
## Simple Proof of the Global Optimality of the Hohmann Transfer
* Minimum-fuel transfer
* First impulse to transfer orbit, second impulse to circularize orbit. Velocity change is parallel to the instantaneous velocity vector
* For $R \equiv r_2/r_1 > 1$, the Hohmann transfer is optimal among two-impulse transfers
* For values of $R > 11.24$, the Hohmann transfer is no longer the global optimal impulsive transfer.
	* A three-impulse [bielliptic transfer](https://en.wikipedia.org/wiki/Bi-elliptic_transfer) can always be found with a lower cost, if the midcourse impulse occurs sufficiently far outside the outer terminal orbit.
* For $R > 15.58$, any bielliptic transfer that has its midcourse impulse outside the outer terminal orbit has a lower cost than the Hohmann transfer.
	* The major disadvantage of the bielliptic transfer is that the transfer time is more than twice the Hohmann transfer time while the cost saving in the coplanar case is modest.
	* In noncoplanar cases, the bielliptic transfer provides significant cost savings because the required plane fchange can be accomplished at larger radius and therefore lower velocity

## The Optimization of the Orbital Hohmann Transfer
* 4 bi-implusive configurations for the generalized Hohmann orbit transfer.
* Apogee: Furthest distance away from orbiting object
* Hohmann configuration: Apogee of transfer orbit is apogee of final orbit:
$$
\begin{array}{c}
	a_1 (1-e_1) = a_T(1-e_t) \\
	a_2 (1-e_2) = a_T(1-e_t) \\
\end{array}
$$

* This transfer assumes the orbit is coplanar

# Kalman Filters
## Understanding the Kalaman Filter (Meinhold)
* Assuming linear relationship between measurable values and dynamics of the system

$$
Y_t = F_t \theta_t + v_t
$$

> where $Y_t$ is the estimated measured input, $F_t$ known, $\theta_t$ is the state at time $t$, and $v_t$ is the observation error.

* These inputs are based on the dynamics of the system

$$
\theta_t = G_t \theta_{t-1} + w_t
$$

> Where $G_t$ is known and $w_t$ is system equation error.

* $w$ is independent of $v$. The observation error is independent of the system equation error
* Look forward in time in two stages:
	* Before observing $Y_t$ (estimate the state)
	* After observing $Y_t$ (measure the state)

## An Introduction to the Kalman Filter (Welch)
* $w$ is the process noise
* $v$ is the measurement noise
* Error before and after measurement (priori and posteriori)
$$
\begin{array}{c}
	e^-_k \equiv x_k - \hat{x^-_k} \\
	e_k \equiv x_k - \hat{x_k}   \\
\end{array}
$$

* The form should represent that when the measurement error is high, trust the residual more. On the other hand, when the measurement error is low trust the measurement
* Kalman filter has two main states:
	* Time update (update model prediction)
	* Measurement update (update measurement)

# Power Ascent
## Linear Covariance Techniques for Powered Ascent (Rose)
* Powered ascent consists of two stages: open and closed loop
	* The open loop portion is employed during the early stages. It consists of predetermined attitude and attitude rate commands to steer the vehicle along a designated ascent profile designed to satisfy in-flight constraints
	* The closed-loop portion operates during the last stage of powered ascent and utilizes the current navigtion state solution along with an onboard model of the system dynamics to compute the fuel-optimal thrust pointing command needed to satisfy terminal or engine cutoff constraints.
* This paper separates the powered ascent profile into three sections:
	* vertical rise
		* Usually lasts only a few seconds
		* Primary purpose is to safely cleaar the launch pad or tower
	* pitch over
		* Typically spans tens of seconds
		* The purpose is to reorient the interial thrust direction from the vertical to the optimal heading for ascent
	* ascent
		* Typically lasts a few minutes and ends at main engine cuttoff
		* Comprised of two guidance models rather than a single mode
			* PEG (Power Explicit Guidance)
			* fine count

## Apollo Experience Report - Mission Planning For Lunar Module Descent and Ascent (Bennet)
* PGNCS: Primay Guidance and Navigation Control System
* RCS: Reaction Control System
* APS: Ascent Propulsion System
* DAP: Digital Autopilot
* Composed of two operational phases:
	* vertical rise
		* Lasts about 10 seconds
	* orbit insertion
* Guidance switches to orbit-insertion when 40 fps is reached.

# SLAM
## A Tutorial on Graph-Based SLAM
* Intuitive way to address the SLAM problem is via graph-based formulation
* It involves constructing a graph whose nodes represent system poses or landmarks and in which an edge between two nodes encodes a sensor measurement that contains the connected poses. (Correlate system measurable data with external world data)
* SLAM took several years to become popular due to the comparably high complexity of solving the error minimization problem using standard techniques
* Landmark maps are often preferred in environments where locally distinguishable feature scan be identified and especially when cameras are used.
	* Use landmarks to localize
* Graph based SLAM, the poses of the robot are modeled by nodes in a graph and labeled with their positions in the environment. Spatial constraints between poses that results from observations or odometry measurements are encoded in the edges between the nodes.
* Data association:
	* requires a consistent estimate of the conditional prior over the robot trajectory

## Simultaneous Localization and Mapping Part II
* Process by which a robot can build a map of the environment and simultaneously compute its location
* SLAM, in its native form, scales quadratically with the number of landmarks in a map

## MonoSLAM: Real-Time Single Camera SLAM
* Until recently the use of cameras has not been at the center of progress in SLAM.
* Structured From Motion (SFM) do not scale to consistent localization over arbitrarily long sequences in real time.
* This work is focused on high frame-rate real-time performance (30 Hz)
* The most useful information when acting in real-time scenarios is its current location.
* This work focuses on the localization as the main output
* Use slam to implement on-the-fly estimation of the state of the moving camera and its map and benefit from this in using the running estimates to guide efficient processing

# Terrain Relative Navigation
## A General Approach to Terrain Relative Navigation for Planetary Landing (Johnson)
* Vision Aided Inertial Navigation (VISINAV) starts by propagating inertial measurements (accelerations and angular rates) from an inertial measurement unit.
* When images begin being collected, a decent image is used to actract matches from a digital elevation map.
* The output of the above two bullets is a set of mapped landmark measurements. They are required to make the position of the lander observable by the filter.
* The filter processes feature tracks obtained in two ways:
	* For a few persistent features the filter adds the global position of each feature to the filter state so that it can be estimated. This has the effect of integrating out noise in the feature track while adding stability to the filter.
	* The remaining opportunistic features are processed in a separate more computationally efficient module that computes a five degree-of-freedom motion estimate (direct translation and rotation) between images. The motion estimate (instead of the images themselves) are used as the input into the filter.
* General landmark expression approaches:
	* Scale Invariant Feature Transform
		* Does not reuly on a geometric landmark model
		* Converts image data into feature vector or keypoint that can be matched between a map and image
		* All tha tis reqruied to generate descriptive SIFT keypoints is the unique image texture (a model of terrain shape is not requried or used).
		* SIFT keypoints can be matched without knowlede of the spacecraft alititude or orientation relative to the surface of the map
	* SIFT is not required
		* Landers always have an altimeter and gyroscopes that provide on-board measurements of altitude and surface-relative attitude.	
		* Using these measurements, descent images can be warped into a local level coordinate system so that the normalized corrleation can be applied to find matches between the descent image and map.
		* Image correlation does not rely on a specific geometric model of landmarks to enable detection
		* Image correlation is more mature and widely accepted than SIFT
	* Utilizing general landmark expression, their global positions are not required.
		* Track them over time and use on-board measurements to track distance traveled and propogate that over time

## Terrain-Relative and Beacon-Relative Navigation for Lunar Powered Descent and Landing
* Two types techniques that were used in ALHAT:
	* Active
		* Laser raning to produce a 3-D terrain map 
	* Passive
		* Used conventional CCD cameras and image processing to either locate craters and coorelate them with an onboard database or to rorrelate rectified images taken by the lander with lunar maps.
		* Features on the map can be tracked between frames to observe the velocity of the sapcecraft relative to the surface.
		* Depending on the quality of the maps and images, the position of the spacecraft can be estimated with an accuracy on the order of 40 m or less.
* Extreme lighting can cause issues with the passive technique (image processing is affected)

## Overview of Terrain Relative Navigation Approaches for Precise Lunar Landing (Johnson)
* Three TRN functions: global position estimation, local position estimation, and velocity estimation.
* Two main categories: 
	* Passive imaging
	* Active rage sensing
* These two groups can be broken down into further subgroups	
	* Global position estimation
	* Local position estimation
